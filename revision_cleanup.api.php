<?php

/**
 * @file
 * Hooks provided by the revisions_cleanup module.
 */

/**
 * Alters the revisions ids to be kept on the database.
 *
 * Use the format $result[nid][vid] where:
 * - nid: is the node id.
 * - vid: is the revision id.
 *
 * @param array $result
 *   An array of revisions ids in the format $result[nid][vid].
 */
function hook_revisions_cleanup_keep_alter(array &$result) {
  // Alter the revisions IDs to be kept on the database by removing or adding
  // other revisions.
  // In this example we are also keeping revisions in draft or in needs review
  // state.
  $revisions_in_draft_or_needs_review = \Drupal::database()->query("
    SELECT content_entity_id AS nid
          ,content_entity_revision_id AS vid
      FROM {content_moderation_state_field_revision}
     WHERE content_entity_type_id = 'node'
       AND moderation_state IN ('draft', 'needs_review')
  ")->fetchAll();

  if (count($revisions_in_draft_or_needs_review)) {
    foreach ($revisions_in_draft_or_needs_review as $item) {
      $result[$item->nid]['nid'] = $item->nid;
      $result[$item->nid]['vids'][$item->vid] = $item->vid;
    }
  }
}
