<?php

namespace Drupal\revision_cleanup\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Queue\QueueFactory;

/**
 * RevisionCleanupService class definition.
 */
class RevisionCleanupService {

  /**
   * The EntityTypeManager service.
   *
   * @var array|\Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  private $logger;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $config;

  /**
   * The ModuleHandler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private ModuleHandlerInterface $moduleHandler;

  /**
   * The QueueFactory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  private QueueFactory $queue;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param \Drupal\Core\Logger\LoggerChannel $logger
   *   The logger channel service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The ModuleHandler service.
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   The QueueFactory service.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, Connection $connection, LoggerChannel $logger, ConfigFactoryInterface $config, ModuleHandlerInterface $module_handler, QueueFactory $queue) {
    $this->entityTypeManager = $entity_manager;
    $this->database = $connection;
    $this->logger = $logger;
    $this->config = $config;
    $this->moduleHandler = $module_handler;
    $this->queue = $queue;
  }

  /**
   * Add revisions to be cleaned up on queue.
   *
   * @param int $keep_revisions_newer_than_in_days
   *   Delete only revisions older than a specified number of days.
   * @param int $number_of_revisions_to_keep_per_month
   *   The number of most recent revisions to keep on each month.
   *
   * @return int
   *   The number of entities added to the queue.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function addEntitiesToQueue(int $keep_revisions_newer_than_in_days, int $number_of_revisions_to_keep_per_month): int {
    // @todo allow other entities?
    $entity_type = 'node';
    $entities = $this->getEntities($entity_type, $keep_revisions_newer_than_in_days, $number_of_revisions_to_keep_per_month);

    $queue = $this->queue->get('revision_cleanup_processor');

    // Before adding the items into the queue, we need to clear the queue to
    // avoid duplicates.
    $queue->deleteQueue();
    foreach ($entities as $entity) {
      // Only add items into queue that have revisions to delete.
      if (count($entity['vids'])) {
        // Add item to queue.
        $queue->createItem($entity);
      }
    }

    return count($entities);
  }

  /**
   * Get the entities data to prepare the queue to remove the revisions.
   *
   * @param string $entity_type
   *   The entity type id - node|block.
   * @param int $keep_revisions_newer_than_in_days
   *   Delete only revisions older than a specified number of days.
   * @param int $number_of_revisions_to_keep_per_month
   *   The number of most recent revisions to keep on each month.
   *
   * @return array
   *   The array with the revisions information to be added on the queue.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEntities(string $entity_type, int $keep_revisions_newer_than_in_days, int $number_of_revisions_to_keep_per_month): array {
    $ids = $this->entityTypeManager
      ->getStorage($entity_type)
      ->getQuery()
      ->accessCheck(FALSE)
      ->execute();

    $result = [];

    if (count($ids)) {
      foreach ($ids as $id) {
        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        $entity = $this->entityTypeManager
          ->getStorage($entity_type)
          ->load($id);

        $result[$id]['entity_type'] = $entity_type;
        $vids = $this->entityTypeManager->getStorage($entity_type)->revisionIds($entity);
        if (count($vids)) {
          foreach ($vids as $vid) {
            $result[$id]['vids'][$vid] = $vid;
          }
        }
        else {
          $result[$id]['vids'] = [];
        }
        $result[$id]['id'] = $id;

        $translation_languages = $entity->getTranslationLanguages();
        if (count($translation_languages)) {
          $translation_languages = array_keys($translation_languages);
          foreach ($translation_languages as $language) {
            $latest_revision_id = $this->entityTypeManager
              ->getStorage($entity_type)
              ->getLatestTranslationAffectedRevisionId($id, $language);

            $result[$id]['default_vids'][] = (int) $latest_revision_id;
            $result[$id]['default_vids'][] = (int) $entity->getRevisionId();
            $result[$id]['default_vids'][] = (int) $this->getDefaultRevisionId($entity_type, $id);
          }
          $result[$id]['default_vids'] = array_unique($result[$id]['default_vids']);
        }
      }
    }

    // Remove revisions to keep.
    $revisions_to_keep = $this->revisionsToKeep($entity_type, $keep_revisions_newer_than_in_days, $number_of_revisions_to_keep_per_month);
    if (count($revisions_to_keep)) {
      foreach ($revisions_to_keep as $item) {
        $vids = $item['vids'];
        if (count($vids)) {
          foreach ($vids as $vid) {
            if (isset($result[$item['nid']]['vids'][$vid])) {
              unset($result[$item['nid']]['vids'][$vid]);
            }
          }
        }
      }
    }

    return $result;
  }

  /**
   * Get the default revision id.
   *
   * @param string $entity_type
   *   The entity type.
   * @param int $entity_id
   *   The entity id.
   *
   * @return int
   *   The default revision id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getDefaultRevisionId(string $entity_type, int $entity_id) {
    $definition = $this->entityTypeManager
      ->getDefinition($entity_type);

    $result = $this->entityTypeManager
      ->getStorage($entity_type)
      ->getQuery()
      ->currentRevision()
      ->condition($definition->getKey('id'), $entity_id)
      ->accessCheck(FALSE)
      ->execute();

    if ($result) {
      return key($result);
    }

    return NULL;
  }

  /**
   * Get the revisions to keep.
   *
   * @param string $entity_type
   *   The entity type.
   * @param int $keep_revisions_newer_than_in_days
   *   Delete only revisions older than a specified number of days.
   * @param int $number_of_revisions_to_keep_per_month
   *   The number of most recent revisions to keep on each month.
   *
   * @return array
   *   The array of revisions to be kept.
   */
  private function revisionsToKeep(string $entity_type, int $keep_revisions_newer_than_in_days, int $number_of_revisions_to_keep_per_month): array {
    $result = [];

    $offset_formatted = '+00:00';
    $offset_seconds = 0;
    if ($this->config->get('revision_cleanup.settings')->get('use_site_timezone')) {
      // Convert the site timezone into hours with sign.
      $site_timezone = $this->config->get('system.date')->get('timezone')['default'] ?? 'UTC';
      $timezone = new \DateTimeZone($site_timezone);
      $offset_seconds = $timezone->getOffset(new \DateTime());
      $offset_hours = $offset_seconds / 3600;
      $offset_sign = ($offset_hours < 0 ? '-' : '+');
      $offset_abs = abs($offset_hours);
      $offset_formatted = sprintf('%s%02d:%02d', $offset_sign, floor($offset_abs), ($offset_abs - floor($offset_abs)) * 60);
    }

    if ($number_of_revisions_to_keep_per_month > 0) {
      $revisions_per_month = $this->database->query("
        SELECT a.nid
             , b.langcode
             , DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(a.revision_timestamp), '+00:00', '$offset_formatted'), '%Y-%m') AS yearmonth
             , GROUP_CONCAT(a.vid SEPARATOR ',') AS year_monthly_vids
          FROM {node_revision} a INNER JOIN {node_field_revision} b ON a.vid = b.vid
         WHERE b.revision_translation_affected = 1
         GROUP BY nid, yearmonth, b.langcode
         ORDER BY nid, yearmonth DESC
    ")->fetchAll();

      if (count($revisions_per_month)) {
        foreach ($revisions_per_month as $item) {
          $result[$item->nid]['nid'] = $item->nid;
          $result[$item->nid]['month'] = $item->yearmonth;
          $result[$item->nid]['langcode'] = $item->langcode;
          $vids = array_map('intval', explode(',', $item->year_monthly_vids));
          rsort($vids);
          $vids = array_slice($vids, 0, $number_of_revisions_to_keep_per_month);
          if (count($vids)) {
            foreach ($vids as $vid) {
              $result[$item->nid]['vids'][$vid] = $vid;
            }
          }
          else {
            $result[$item->nid]['vids'] = [];
          }
        }
      }
    }

    if ($keep_revisions_newer_than_in_days > 0) {
      $timestamp = time() - ($keep_revisions_newer_than_in_days * 24 * 60 * 60);

      // Adjust the timestamp to Site timezone.
      $timestamp += $offset_seconds;

      $latest_revisions = $this->database->query("
      SELECT nid
           , vid
        FROM {node_revision}
       WHERE revision_timestamp >= $timestamp
       ORDER BY nid, vid DESC
    ")->fetchAll();

      if (count($latest_revisions)) {
        foreach ($latest_revisions as $item) {
          $result[$item->nid]['nid'] = $item->nid;
          $result[$item->nid]['vids'][$item->vid] = $item->vid;
        }
      }
    }

    // If an alter hook wants to modify the revision ids to keep on database, it
    // can be done using hook_revisions_cleanup_keep_alter.
    $this->moduleHandler->alter(['revisions_cleanup_keep'], $result);

    return $result;
  }

}
