<?php

namespace Drupal\revision_cleanup\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Clean up revisions.
 *
 * @QueueWorker(
 *   id = "revision_cleanup_processor",
 *   title = @Translation("Task Worker: Clean up entity revisions"),
 *   cron = {"time" = 60}
 * )
 */
class RevisionCleanUpQueueProcessor extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|mixed
   */
  private $config;

  /**
   * The EntityTypeManager service.
   *
   * @var array|\Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannel|string
   */
  private $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, LoggerChannel $logger, ConfigFactoryInterface $config) {
    $this->entityTypeManager = $entity_manager;
    $this->logger = $logger;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.channel.revision_cleanup'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (isset($data['vids'])) {
      $entity_type = $data['entity_type'];
      $vids = $data['vids'];
      if (count($vids)) {
        foreach ($vids as $vid) {
          // Do not delete default revisions.
          if (!in_array($vid, $data['default_vids'])) {
            $this->entityTypeManager->getStorage($entity_type)
              ->deleteRevision($vid);

            if ($this->config->get('revision_cleanup.settings')->get('logger')) {
              $this->logger->info('Revision vid:' . $vid . ' cleaned on ' . $entity_type . ' id:' . $data['id']);
            }
          }
        }
      }
    }
  }

}
