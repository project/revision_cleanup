<?php

namespace Drupal\revision_cleanup\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements RevisionCleanupSettingsForm class.
 */
class RevisionCleanupSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'revision_cleanup_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['revision_cleanup.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('revision_cleanup.settings');

    $form = parent::buildForm($form, $form_state);

    $form['days_to_keep'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of days'),
      '#description' => $this->t('For each content, keep all revisions newer of a certain number of days.'),
      '#min' => 0,
      '#default_value' => $config->get('days_to_keep') ?? 90,
      '#required' => TRUE,
    ];

    $form['revisions_per_month'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of revisions to keep per month'),
      '#description' => $this->t('For each content, on each month, keep a certain number of revisions.'),
      '#min' => 0,
      '#default_value' => $config->get('revisions_per_month') ?? 1,
      '#required' => TRUE,
    ];

    $form['use_site_timezone'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use site timezone instead of UTC.'),
      '#description' => $this->t('By default the UTC timezone is considered to select the latest revision of the month. By checking this option, the site timezone will be considered instead of the UTC.'),
      '#default_value' => (bool) $config->get('use_site_timezone') ?? 0,
    ];

    $form['on'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Run on cron'),
      '#default_value' => (bool) $config->get('on') ?? 0,
    ];

    $form['logger'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log activities on watchdog'),
      '#default_value' => (bool) $config->get('logger') ?? 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('revision_cleanup.settings');
    $config->set('days_to_keep', $form_state->getValue('days_to_keep'))
      ->set('revisions_per_month', $form_state->getValue('revisions_per_month'))
      ->set('use_site_timezone', $form_state->getValue('use_site_timezone'))
      ->set('on', (bool) $form_state->getValue('on'))
      ->set('logger', (bool) $form_state->getValue('logger'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
